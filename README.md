# Java Dockerfile


This repository contains **Dockerfile** of [Java](https://www.java.com/) for [Docker](https://www.docker.com/)'s [automated build](https://registry.hub.docker.com/u/nuccess/nuclos-jdk/) published to the public [Docker Hub Registry](https://registry.hub.docker.com/).

## Base Docker Image

* [ubuntu/14.04](https://hub.docker.com/_/ubuntu/)

## Docker Tags

* `latest` (default)
* `1.8.0_171
* `1.8.0_151
* `1.8.0_144`
* `1.8.0_131`

# Usage

**This container is NOT required for a productive environment!** 

Build processes of the Nuclos container loads it automatically when needed.